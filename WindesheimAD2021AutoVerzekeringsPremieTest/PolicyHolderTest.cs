using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest
    {
        [Theory]
        [InlineData(20, "01-04-2030", 1234, 2, -9)]
        [InlineData(30, "01-06-2010", 1013, 5, 11)]
       
        public void LicenseAgeOfPolicyHolderIsCalculatedCorrectly(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears, int expected)
            {
                // Arrange
                PolicyHolder policyHolder = new(Age, DriverlicenseStartDate, PostalCode, NoClaimYears);

                // Act
                int result = policyHolder.LicenseAge;

                // Assert
                Assert.Equal(expected, result);
            }
    }
}
