﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class VehicleTest
    {
        [Fact]
        public void VehicleAgeIsCalculatedCorrectly()
        {
            //Arrange
            var vehicleAge = 7;
            var constructionYear = DateTime.Now.Year - vehicleAge;

            //Act
            var vehicle = new Vehicle(1, 1, constructionYear);

            //Assert
            Assert.Equal(vehicleAge, vehicle.Age);
        }
    }
}
