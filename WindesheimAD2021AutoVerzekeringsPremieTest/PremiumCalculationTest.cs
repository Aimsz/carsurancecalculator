﻿using System;
using System.Collections.Generic;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {
        [Theory]
        [InlineData(50, 10000, 6)]
        [InlineData(20, 5000, 2)]
        [InlineData(0, 0, 5)]
        public void PremiumCalculationBasePremiumCalculatedCorrectly(int powerInKw, int valueinEuros, int age)
        {
            // Arrange
            var mockVehicle = new Mock<IVehicle>();
            mockVehicle.Setup(m => m.PowerInKw).Returns(powerInKw);
            mockVehicle.Setup(m => m.ValueInEuros).Returns(valueinEuros);
            mockVehicle.Setup(m => m.Age).Returns(age);

            var expected = ((double)valueinEuros / 100 - age + (double)powerInKw / 5) / 3;
            var expectedValue = Math.Round(expected, 2);

            // Act
            var calculation = PremiumCalculation.CalculateBasePremium(mockVehicle.Object);

            // Assert
            Assert.Equal(expectedValue, Math.Round(calculation, 2));
        }

        [Fact]
        public void MonthlyPremiumPaymentAmountCalculatedCorrectly()
        {
            // Arrange
            var vehicle = new Vehicle(20, 12000, 2012);
            var policyHolder = new PolicyHolder(25, "01-06-2015", 1045, 5);
            
            var expected = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR) / 12;

            //Act
            var actual = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(Math.Round(expected), Math.Round(actual));
        }

        [Theory]
        // Testing limit values. 15% raise if the age is < 23
        [InlineData(22, 6, 1.15)]
        [InlineData(23, 6, 1.00)]
        [InlineData(24, 6, 1.00)]
        // Testing limit values. 15% raise if drivers license is < 5
        [InlineData(30, 4, 1.15)]
        [InlineData(30, 5, 1.00)]
        [InlineData(30, 6, 1.00)]
        public void PremiumRaiseIfYoungerThan23YearsOrLicenseAgeBelow5YearsCalculation(int age, int licenseAge, double expected)
        {
            // Arrange
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(x => x.PowerInKw).Returns(20);
            vehicle.Setup(x => x.ValueInEuros).Returns(12000);
            vehicle.Setup(x => x.Age).Returns(2);

            var policyHolder = new Mock<IPolicyHolder>();
            policyHolder.Setup(p => p.Age).Returns(age);
            policyHolder.Setup(p => p.LicenseAge).Returns(licenseAge);
            policyHolder.Setup(p => p.PostalCode).Returns(7000);
            policyHolder.Setup(p => p.NoClaimYears).Returns(2);
            
            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle.Object);
            var expectedRaise = expected * basePremium;

            //Act
            var actual = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA).PremiumAmountPerYear;

            //Assert
            Assert.Equal(Math.Round(expectedRaise, 2), Math.Round(actual, 2));
        }

        [Theory]
        // Testing limit values
        [InlineData(999, 1.00)]
        [InlineData(1200, 1.05)]
        [InlineData(3600, 1.02)]
        [InlineData(4499, 1.02)]
        [InlineData(4500, 1.00)]
        [InlineData(4600, 1.00)]
        public void PremiumIsCalculatedCorrectlyBasedOnPostalCode(int postalCode, double expected)
        {
            // Arrange
            var vehicle = new Vehicle(20, 12000, 2012);
            var policyholder = new PolicyHolder(30, "01-01-2001", postalCode, 0);

            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            var expectedRaise = expected * basePremium;
            
            // Act
            var actual = new PremiumCalculation(vehicle, policyholder, InsuranceCoverage.WA).PremiumAmountPerYear;

            //Assert
            Assert.Equal(expectedRaise, actual);
        }

        [Theory]
        [InlineData(0, 1.00)]
        [InlineData(5, 1.00)]
        [InlineData(6, 0.95)]
        [InlineData(10, 0.75)]
        public void PremiumIsCalculatedBasedOnNoClaimYearsDiscount(int noClaimYears, double expectedDiscount)
        {
            // Arrange
            var vehicle = new Vehicle(44, 7000, 2017);
            var policyHolder = new PolicyHolder(30, "01-01-2010", 8000, noClaimYears); ;

            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            var expected = basePremium * expectedDiscount;

            // Act
            var actual = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA).PremiumAmountPerYear;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WaPlusRaiseCalculation()
        {
            // Arrange
            var vehicle = new Vehicle(20, 12000, 2012);
            var policyholder = new PolicyHolder(30, "01-01-2001", 7000, 0);
            double premiumRaise = 1.20;

            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            var expected = basePremium * premiumRaise;

            // Act
            var actual = new PremiumCalculation(vehicle, policyholder, InsuranceCoverage.WA_PLUS).PremiumAmountPerYear;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AllRiskRaiseCalculation()
        {
            // Arrange
            var vehicle = new Vehicle(20, 12000, 2012);
            var policyholder = new PolicyHolder(30, "01-01-2001", 7000, 0);
            double premiumRaise = 2.00;

            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            var expected = basePremium * premiumRaise;

            // Act
            var actual = new PremiumCalculation(vehicle, policyholder, InsuranceCoverage.ALL_RISK).PremiumAmountPerYear;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
