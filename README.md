## Deel 1 & 2

Beschrijving van de unittesten & eventuele gevonden fouten


## PolicyHolderTest

#### LicenseAgeofPolicyHolderIsCalculatedCorrectly

Bij deze test maak ik gebruik van een [Theory] om na te gaan of de licenseage van de policyholder goed wordt berekend.
Bij de eerste inlinedata maak ik gebruik van een datum die nog niet geweest is, waardoor de uitkomst negatief zou moeten zijn.
Bij de tweede inlinedata maak ik gebruik van random gekozen data waarbij de uitkomst positief zou moeten zijn.

* Geen fouten gevonden

## VehicleTest

#### VehicleAgeIsCalculatedCorrectly

Bij deze test heb ik gebruik gemaakt van een [Fact] om na te gaan of de vehicle age goed wordt berekend.

* Geen fouten gevonden

## PremiumCalculationTest

#### PremiumCalculationBasePremiumCalculatedCorrectly

Bij deze test heb ik gebruik gemaakt van een [Theory] en een mock om na te gaan of de premium base goed wordt berekend.
Om gebruik te maken van de mock heb ik interface IVehicle toegevoegd in de Vehicle klasse.

Gevonden fouten:
* In de CalculateBasePremium wordt er geen gebruik gemaakt van haakjes tijdens de berekening, deze heb ik toegevoegd.
* In de CalculateBasePremium werd een int meegegeven ipv een double, dit heb ik ook veranderd.

####  PremiumRaiseIfYoungerThan23YearsOrLicenseAgeBelow5YearsCalculation

Gebruik gemaakt van grenswaarde analyse met behulp van een [Theory] en een mock om na te gaan of er een 15% raise bij komt als de license age < 5 is of als de leeftijd < 23 is.

Gevonden fouten:
* In het requirements document staat aangegeven dat er een premie-opslag van 15% is bij een rijbewijs korter dan 5 jaar. 
In de premiumcalculation staat kleiner dan of gelijk aan (<=) 5 jaar. Dit heb ik veranderd naar < 5

#### PremiumIsCalculatedCorrectlyBasedOnPostalCode

Gebruik gemaakt van grenswaarde analyse met behulp van een [Theory] om na te gaan of het percentage risico opslag goed wordt berekend op basis van de postalCode.
5% opslag voor de postcodes >= 1000 en < 3600
2% opslag voor de postcodes >= 3600 en < 4500

Gevonden fouten:
* In de 2% opslag stond alleen < 4500. Terwijl in de requirements stond dat het >= 3600 en < 4500 moet zijn. Dit heb ik veranderd.


#### PremiumIsCalculatedBasedOnNoClaimYearsDiscount

Gebruik gemaakt van grenswaarde analyse met behulp van een [Theory] om na te gaan of het discount percentage goed wordt toegepast op de premiumprijs.

Gevonden fouten:
* NoClaimPercentage van een int naar een double veranderd. Anders is de uitkomst van de premium 0. 

#### WaPlusRaiseCalculation & AllRiskRaiseCalculation

Hierbij heb ik gebruik gemaakt van een [Fact] om te controleren of de toeslag voor de WA plus (20%)  en All risk (dubbele van WA) goed worden berekend

* Geen fouten gevonden










